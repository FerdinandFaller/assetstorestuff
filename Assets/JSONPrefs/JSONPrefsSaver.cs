using UnityEngine;

public class JSONPrefsSaver : MonoBehaviour
{
    private float _saveInterval;
    public float SaveInterval
    {
        get { return _saveInterval; }
        set
        {
            _saveInterval = value;
            _lastSave = Time.realtimeSinceStartup;
        }
    }

    private bool _saveOnInterval;
    public bool SaveOnInterval
    {
        get { return _saveOnInterval; }
        set
        {
            _saveOnInterval = value;
            _lastSave = Time.realtimeSinceStartup;
        }
    }

    public bool SaveOnApplicationQuit = true;
    public bool SaveOnApplicationPause = true;

    private float _lastSave;


    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (!SaveOnInterval || !(Time.realtimeSinceStartup - _lastSave > SaveInterval)) return;
        _lastSave = Time.realtimeSinceStartup;

        JSONPrefs.Save();
    }

    void OnApplicationQuit()
    {
        if(SaveOnApplicationQuit) JSONPrefs.Save();
    }

    void OnApplicationPause()
    {
        if(SaveOnApplicationPause) JSONPrefs.Save();
    }
}
