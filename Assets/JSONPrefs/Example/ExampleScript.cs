using UnityEngine;

public class ExampleScript : MonoBehaviour 
{
	void Start ()
	{
		const string testString = "TestString";
		const int testInt = 12345;
		const float testFloat = 1.2345f;
		var playerPrefsTotal = 0f;
		var jsonPrefsTotal = 0f;

		// Write into PlayerPrefs
		var playerPrefsTime = Time.realtimeSinceStartup;
		for (var i = 0; i < 2000; i++)
		{
			PlayerPrefs.SetInt("Int" + i, testInt);
			PlayerPrefs.SetFloat("Float" + i, testFloat);
			PlayerPrefs.SetString("String" + i, testString);
		}
		playerPrefsTime = Mathf.Clamp(Time.realtimeSinceStartup - playerPrefsTime, 0.0001f, 1000f);
		playerPrefsTotal += playerPrefsTime;
		Debug.Log("PlayerPrefs write time: " + playerPrefsTime);

		// Save PlayerPrefs
		playerPrefsTime = Time.realtimeSinceStartup;
		PlayerPrefs.Save();
		playerPrefsTime = Mathf.Clamp(Time.realtimeSinceStartup - playerPrefsTime, 0.0001f, 1000f);
		playerPrefsTotal += playerPrefsTime;
		Debug.Log("PlayerPrefs save time: " + playerPrefsTime);

		// Read from PlayerPrefs
		playerPrefsTime = Time.realtimeSinceStartup;
		for (var i = 0; i < 2000; i++)
		{
			PlayerPrefs.GetInt("Int" + i);
			PlayerPrefs.GetFloat("Float" + i);
			PlayerPrefs.GetString("String" + i);
		}
		playerPrefsTime = Mathf.Clamp(Time.realtimeSinceStartup - playerPrefsTime, 0.0001f, 1000f);
		playerPrefsTotal += playerPrefsTime;
		Debug.Log("PlayerPrefs read time: " + playerPrefsTime);

		Debug.LogWarning("PlayerPrefs Total: " + playerPrefsTotal);



		// Write into JSONPrefs
		var jsonPrefsTime = Time.realtimeSinceStartup;
		for (var i = 0; i < 2000; i++)
		{
			JSONPrefs.SetInt("Int" + i, testInt);
			JSONPrefs.SetFloat("Float" + i, testFloat);
			JSONPrefs.SetString("String" + i, testString);
		}
		jsonPrefsTime = Mathf.Clamp(Time.realtimeSinceStartup - jsonPrefsTime, 0.0001f, 1000f);
		jsonPrefsTotal += jsonPrefsTime;
		Debug.Log("JSONPrefs write time: " + jsonPrefsTime);

		// Save JSONPrefs
		jsonPrefsTime = Time.realtimeSinceStartup;
		JSONPrefs.Save();
		jsonPrefsTime = Mathf.Clamp(Time.realtimeSinceStartup - jsonPrefsTime, 0.0001f, 1000f);
		jsonPrefsTotal += jsonPrefsTime;
		Debug.Log("JSONPrefs save time: " + jsonPrefsTime);

		// Read from JSONPrefs
		jsonPrefsTime = Time.realtimeSinceStartup;
		for (var i = 0; i < 2000; i++)
		{
			JSONPrefs.GetInt("Int" + i);
			JSONPrefs.GetFloat("Float" + i);
			JSONPrefs.GetString("String" + i);
		}
		jsonPrefsTime = Mathf.Clamp(Time.realtimeSinceStartup - jsonPrefsTime, 0.0001f, 1000f);
		jsonPrefsTotal += jsonPrefsTime;
		Debug.Log("JSONPrefs read time: " + jsonPrefsTime);

		Debug.LogWarning("JSONPrefs Total: " + jsonPrefsTotal);
	}
}
