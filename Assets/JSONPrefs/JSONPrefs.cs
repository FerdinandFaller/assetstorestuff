using System.Collections.Generic;
using System.IO;
using System.Linq;
using LitJson;
using UnityEngine;

public static class JSONPrefs
{
    private static bool _usePrettyPrint;
    private static Dictionary<string, int> _ints = new Dictionary<string, int>();
    private static Dictionary<string, double> _floats = new Dictionary<string, double>();
    private static Dictionary<string, string> _strings = new Dictionary<string, string>();
    private static Dictionary<string, bool> _bools = new Dictionary<string, bool>();
    private static readonly JSONPrefsSaver Saver;


    static JSONPrefs()
    {
        ReadInts();
        ReadFloats();
        ReadStrings();
        ReadBools();

        // Create the JSONPrefsSaver
        GameObject saverGo = null;
        if (Saver == null) saverGo = new GameObject("JSONPrefsSaver", typeof (JSONPrefsSaver));
        if (saverGo != null) Saver = saverGo.GetComponent<JSONPrefsSaver>();

        Configure();
    }

    #region Private Methods

    private static void ReadInts()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/Ints.txt";

        if (!File.Exists(path)) return;

        var json = File.ReadAllText(path);
        _ints = JsonMapper.ToObject<Dictionary<string, int>>(json);
    }

    private static void WriteInts()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/";

        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        var sw = File.CreateText(path + "Ints.txt");
        var writer = new JsonWriter(sw) {PrettyPrint = _usePrettyPrint};
        JsonMapper.ToJson(_ints, writer);
        sw.Close();
    }

    private static void ReadFloats()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/Floats.txt";

        if (!File.Exists(path)) return;

        var json = File.ReadAllText(path);
        _floats = JsonMapper.ToObject<Dictionary<string, double>>(json);
    }

    private static void WriteFloats()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/";

        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        var sw = File.CreateText(path + "Floats.txt");
        var writer = new JsonWriter(sw) { PrettyPrint = _usePrettyPrint };
        JsonMapper.ToJson(_floats, writer);
        sw.Close();
    }

    private static void ReadStrings()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/Strings.txt";

        if (!File.Exists(path)) return;

        var json = File.ReadAllText(path);
        _strings = JsonMapper.ToObject<Dictionary<string, string>>(json);
    }

    private static void WriteStrings()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/";

        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        var sw = File.CreateText(path + "Strings.txt");
        var writer = new JsonWriter(sw) { PrettyPrint = _usePrettyPrint };
        JsonMapper.ToJson(_strings, writer);
        sw.Close();
    }

    private static void ReadBools()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/Bools.txt";

        if (!File.Exists(path)) return;

        var json = File.ReadAllText(path);
        _bools = JsonMapper.ToObject<Dictionary<string, bool>>(json);
    }

    private static void WriteBools()
    {
        var path = Application.persistentDataPath + "/JSONPrefs/";

        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        var sw = File.CreateText(path + "Bools.txt");
        var writer = new JsonWriter(sw) { PrettyPrint = _usePrettyPrint };
        JsonMapper.ToJson(_bools, writer);
        sw.Close();
    }

    private static bool HasIntKey(string key)
    {
        return _ints.ContainsKey(key);
    }

    private static bool HasFloatKey(string key)
    {
        return _floats.ContainsKey(key);
    }

    private static bool HasStringKey(string key)
    {
        return _strings.ContainsKey(key);
    }

    private static bool HasBoolKey(string key)
    {
        return _bools.ContainsKey(key);
    }

    #endregion // Private Methods

    #region Public Methods

    /// <summary>
    /// Use this to configure how JSONPrefs should handle saving. Gets automatically called with the default values by the constructor.
    /// </summary>
    /// <param name="usePrettyPrint">Should your Data be nicely readable in the .txt file? Default = true.</param>
    /// <param name="saveOnApplicationPause">Should your Data be saved when the Application gets paused? Default = true.</param>
    /// <param name="saveOnApplicationQuit">Should your Data be saved when the Application gets terminated? Default = true.</param>
    /// <param name="saveOnInterval">Should your Data be regularly saved? Default = false</param>
    /// <param name="saveInterval">The Interval (in seconds), your Data gets saved when saveOnInterval is true. Default = 60.</param>
    public static void Configure(bool usePrettyPrint = true, bool saveOnApplicationPause = true, bool saveOnApplicationQuit = true, bool saveOnInterval = false, float saveInterval = 60)
    {
        _usePrettyPrint = usePrettyPrint;

        if (Saver == null) return;
        Saver.SaveOnApplicationPause = saveOnApplicationPause;
        Saver.SaveOnApplicationQuit = saveOnApplicationQuit;
        Saver.SaveOnInterval = saveOnInterval;
        Saver.SaveInterval = saveInterval;
    }

    /// <summary>
    /// Use this to get access to the internal integer collection (ReadOnly).
    /// </summary>
    /// <returns>Returns the internal integer Dictionary (ReadOnly).</returns>
    public static ReadOnlyDictionary<string, int> GetReadOnlyIntCollection()
    {
        return new ReadOnlyDictionary<string, int>(_ints);
    }

    /// <summary>
    /// Use this to retrieve an integer (System.Int32).
    /// </summary>
    /// <param name="key">The key of the integer you want to retrieve.</param>
    /// <param name="defaultValue">The value that is returned when the key isn't found. Default = 0.</param>
    /// <returns>Returns either the integer which is associated with the given key, or the default value.</returns>
    public static int GetInt(string key, int defaultValue = 0)
    {
        int value;
        return _ints.TryGetValue(key, out value) ? value : defaultValue;
    }

    /// <summary>
    /// Use this to set/add an integer (System.Int32). Adding when the key isn't already associated.
    /// </summary>
    /// <param name="key">The key of the integer you want to set/add.</param>
    /// <param name="value">The integer you want to set/add.</param>
    public static void SetInt(string key, int value)
    {
        if (HasIntKey(key))
        {
            _ints[key] = value;
        }
        else
        {
            _ints.Add(key, value);
        }
    }

    /// <summary>
    /// Use this to get access to the internal float collection (ReadOnly).
    /// </summary>
    /// <returns>Returns the internal float Dictionary (ReadOnly).</returns>
    public static ReadOnlyDictionary<string, float> GetReadOnlyFloatCollection()
    {
        var floatDict = _floats.ToDictionary(keyValuePair => keyValuePair.Key, keyValuePair => (float) keyValuePair.Value);

        return new ReadOnlyDictionary<string, float>(floatDict);
    }

    /// <summary>
    /// Use this to retrieve a float (System.Single).
    /// </summary>
    /// <param name="key">The key of the float you want to retrieve.</param>
    /// <param name="defaultValue">The value that is returned when the key isn't found. Default = 0f.</param>
    /// <returns>Returns either the float which is associated with the given key, or the default value.</returns>
    public static float GetFloat(string key, float defaultValue = 0f)
    {
        double value;
        return _floats.TryGetValue(key, out value) ? (float) value : defaultValue;
    }

    /// <summary>
    /// Use this to set/add a float (System.Single). Adding when the key isn't already associated.
    /// </summary>
    /// <param name="key">The key of the float you want to set/add.</param>
    /// <param name="value">The float you want to set/add.</param>
    public static void SetFloat(string key, float value)
    {
        if (HasFloatKey(key))
        {
            _floats[key] = value;
        }
        else
        {
            _floats.Add(key, value);
        }
    }

    /// <summary>
    /// Use this to get access to the internal string collection (ReadOnly).
    /// </summary>
    /// <returns>Returns the internal integer Dictionary (ReadOnly)</returns>
    public static ReadOnlyDictionary<string, string> GetReadOnlyStringCollection()
    {
        return new ReadOnlyDictionary<string, string>(_strings);
    }

    /// <summary>
    /// Use this to retrieve a string (System.String).
    /// </summary>
    /// <param name="key">The key of the string you want to retrieve</param>
    /// <param name="defaultValue">The value that is returned when the key isn't found. Default = ""</param>
    /// <returns>Returns either the string which is associated with the given key, or the default value.</returns>
    public static string GetString(string key, string defaultValue = "")
    {
        string value;
        return _strings.TryGetValue(key, out value) ? value : defaultValue;
    }

    /// <summary>
    /// Use this to set/add a string (System.String). Adding when the key isn't already associated.
    /// </summary>
    /// <param name="key">The key of the string you want to set/add.</param>
    /// <param name="value">The string you want to set/add.</param>
    public static void SetString(string key, string value)
    {
        if (HasStringKey(key))
        {
            _strings[key] = value;
        }
        else
        {
            _strings.Add(key, value);
        }
    }

    /// <summary>
    /// Use this to get access to the internal bool collection (ReadOnly).
    /// </summary>
    /// <returns>Returns the internal integer Dictionary (ReadOnly)</returns>
    public static ReadOnlyDictionary<string, bool> GetReadOnlyBoolCollection()
    {
        return new ReadOnlyDictionary<string, bool>(_bools);
    }

    /// <summary>
    /// Use this to retrieve a bool (System.Boolean).
    /// </summary>
    /// <param name="key">The key of the bool you want to retrieve</param>
    /// <param name="defaultValue">The value that is returned when the key isn't found. Default = false</param>
    /// <returns>Returns either the bool which is associated with the given key, or the default value.</returns>
    public static bool GetBool(string key, bool defaultValue = false)
    {
        bool value;
        return _bools.TryGetValue(key, out value) ? value : defaultValue;
    }

    /// <summary>
    /// Use this to set/add a bool (System.Boolean). Adding when the key isn't already associated.
    /// </summary>
    /// <param name="key">The key of the bool you want to set/add.</param>
    /// <param name="value">The bool you want to set/add.</param>
    public static void SetBool(string key, bool value)
    {
        if (HasBoolKey(key))
        {
            _bools[key] = value;
        }
        else
        {
            _bools.Add(key, value);
        }
    }

    /// <summary>
    /// Use this to check if a given key is already associated.
    /// </summary>
    /// <param name="key">The key you want to check.</param>
    /// <returns>Returns true if the key is already associated, otherwise false.</returns>
    public static bool HasKey(string key)
    {
        return HasIntKey(key) ||
               HasFloatKey(key) ||
               HasStringKey(key) ||
               HasBoolKey(key);
    }

    /// <summary>
    /// Use this to delete a given key and its value.
    /// </summary>
    /// <param name="key">The key you want to delete.</param>
    public static void DeleteKey(string key)
    {
        if (HasIntKey(key))
        {
            _ints.Remove(key);
        }

        if (HasFloatKey(key))
        {
            _floats.Remove(key);
        }

        if (HasStringKey(key))
        {
            _strings.Remove(key);
        }

        if (HasBoolKey(key))
        {
            _bools.Remove(key);
        }
    }

    /// <summary>
    /// Use this if you want to delete all keys and values.
    /// </summary>
    public static void DeleteAll()
    {
        _ints.Clear();
        _floats.Clear();
        _strings.Clear();
        _bools.Clear();
    }

    /// <summary>
    /// Use this to save all keys and values to disc.
    /// </summary>
    public static void Save()
    {
        WriteInts();
        WriteFloats();
        WriteStrings();
        WriteBools();
    }

    #endregion // Public Methods
}