
-- (GUI-)ScriptManager V1.01 --

Creator: Ferdinand Faller
Image: renjith krishnan / FreeDigitalPhotos.net


Description:

The (GUI-)ScriptManager helps you to organize your (GUI-)Scripts while still being in full control of everything. 
Originally it was designed to be used with Scripts that use the Unity GUI Class, but it is possible to use 
it with any sort of Unity Script.


Features:

- Call all your (GUI-)Scripts from a centralized place
- Easy to handle
- A History, that can be traversed backwards
- A simple and easy to use Focus System
- You keep full control of your Scripts


First Steps:
	
- Open the ExampleScene in (GUI-)ScriptManager/Scenes.

- Now you can see the "ScriptManager" Game Object in the SceneHierarchy. The sole purpose of this Game Object 
  is to hold the (GUI-)ScriptManager Script. The Script itself takes care that the Game Object won't be deleted when, for example, 
  a new Scene is loaded. In your own Projects you must include this Game Object before you can use the 
  (GUI-)ScriptManager. The Prefab "ScriptManager" in (GUI-)ScriptManager/Prefabs/ was added for your convenience.
  
- The "Example" Game Object is only necessary in this special case. It holds the "Init" Script that tells 
  the (GUI-)ScriptManager which GUI-Script it should add first. Normally you would do this directly from 
  one of your own Scripts and not from a seperate "Init" Script.
  
- Please feel free to press Play and take a look at the included Examples in action.


Examples:

- "First": The Start of the Example. Here you can either enable the "Second" or the "Third" Script. 
  The "First" Script is necessary to show the EnablePreviousScript() Method, which is used in the "Third" Script.
  Also you can see the use of the AddHistoryExclusion() Method.

- "Second": This Script shows the use of the GetFocusScript() Method if you enable the "Overlay" Script. Additionally,
  the "Overlay" Script directly manipulates a member in the "Second" Script as soon as "Overlay" gets enabled.

- "Overlay": The Overlay is basically an extension to the "Second" Script. It manipulates a member of the "Second" 
  Script by using the GetEnabledScript<T>() Method.

- "Third": In this Script you can go back to the Script you came from. It uses the EnablePreviousScript() Method.