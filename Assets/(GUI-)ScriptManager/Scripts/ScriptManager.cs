// Copyright (c) 2011 Ferdinand Faller
// Please direct any bugs/comments/suggestions to Ferdinand.Faller@gmail.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using System.Collections.Generic;

public class ScriptManager : MonoBehaviour
{
	#region Public Members
	
	// You can access the Game Object of the ScriptManager directly (without searching for it via .Find) by using this static variable
	public static GameObject ManagerGO;
	
	#endregion
	
	#region Private Members
	
	private static readonly List<string> FocusOrder = new List<string>();
	private static readonly List<string> FocusExclusions = new List<string>();
	private static readonly List<string> History = new List<string>();
	private static readonly List<string> HistoryExclusions = new List<string>();
	private static bool _revertToPrevious;
	
	private const int HistorySize = 10; // You can adjust this value if you need a bigger or smaller history
	
	#endregion
	
	#region Initialization
	
	public void Awake()
	{
		// The GUIManager has to stay alive
		DontDestroyOnLoad(this);
		
		// Important: If you rename the GameObject, dont forget to adjust this string
		ManagerGO = GameObject.Find("ScriptManager");
	}
	
	#endregion
	
	#region Public Methods
	
    /// <summary>
    /// Enables the Script that matches with the given ScriptName.
    /// </summary>
    /// <param name="scriptName">The name of the Script you want to enable.</param>
	public static void EnableScript(string scriptName)
	{
		// Save the current script in the history
		if(FocusOrder.Count > 0 && 
		   !_revertToPrevious && 
		   !HistoryExclusions.Contains(scriptName))
		{
			for(int i = FocusOrder.Count - 1; i > -1; i--)
			{
				if(!HistoryExclusions.Contains(FocusOrder[i]))
				{
					History.Add(FocusOrder[i]);
					break;
				}
			}
		}
		
		_revertToPrevious = false;
		
		// Keep the correct Historysize
		while(History.Count > HistorySize)
		{
			History.RemoveAt(0);
			Debug.Log("History got too big. Deleting the oldest entry.");
		}
		
		if(ManagerGO != null && !IsScriptEnabled(scriptName))
		{
			// Save the current script in the FocusOrder
			if(!FocusExclusions.Contains(scriptName))
			{
				FocusOrder.Add(scriptName);
			}
			
			ManagerGO.AddComponent(scriptName);
		}
		else
		{
			// The Script ist already a component, and got called a second time. So we give it the focus by readding.
			FocusOrder.Remove(scriptName);
			FocusOrder.Add(scriptName);
		}
	}
	
	/// <summary>
    /// Enables the Script that matches with the given ScriptName.
	/// </summary>
    /// <param name="scriptName">The name of the Script you want to disable.</param>
	public static void DisableScript(string scriptName)
	{
		if(ManagerGO != null && IsScriptEnabled(scriptName))
		{
			FocusOrder.Remove(scriptName);
			
			Destroy(ManagerGO.GetComponent(scriptName));
		}
	}

    /// <summary>
    /// Lets you access a currently enabled Script directly.
    /// </summary>
    /// <typeparam name="T">The type of the Script you want to get.</typeparam>
    /// <returns>Null, if the given Scripttype isn't enabled at the moment.</returns>
    public static T GetEnabledScript<T>() where T : Component
    {
        T returnValue = null;

        if (ManagerGO != null)
        {
            returnValue = ManagerGO.GetComponent<T>();
        }

        return returnValue;
    }
	
	/// <summary>
	/// Checks if the Script with the given ScriptName is enabled at the moment.
	/// </summary>
    /// <param name="scriptName">The name of the Script you want to check.</param>
	/// <returns>True, if the Script is enabled. False, if the Script is not enabled.</returns>
	public static bool IsScriptEnabled(string scriptName)
	{
	    return ManagerGO != null && ManagerGO.GetComponent(scriptName) != null;
	}

    /// <summary>
    /// Use this Method to get the Script that currently has the Focus (was enabled last, while not in the Exclusion List).
    /// </summary>
    /// <returns>The Script that currently has the Focus.</returns>
    public static string GetFocusScript()
	{
		string scriptName = "";
	
		if(FocusOrder.Count > 0)
		{
			scriptName = FocusOrder[FocusOrder.Count - 1];
		}
		
		return scriptName;
	}
	
	/// <summary>
	/// Enables the last Script in the History, and removes it from the History. 
	/// The last Script in the History is always the second last Script that was enabled, as long as it isn't in the Exclusion List.
	/// </summary>
	public static void EnablePreviousScript()
	{
		_revertToPrevious = true;
	
		if(History.Count > 0)
		{
			EnableScript(History[History.Count - 1]);
			History.RemoveAt(History.Count - 1);
		}
	}
	
	#region Exclusion Methods
	
    /// <summary>
    /// Adds the Script that matches the given ScriptName to the Focus Exclusion List. That Script won't be recognized by the GetFocusScript() Method.
    /// </summary>
    /// <param name="scriptName">The name of the Script you want to add to the Focus Exclusion List</param>
	public static void AddFocusExclusion(string scriptName)
	{
		if(!FocusExclusions.Contains(scriptName))
		{
			FocusExclusions.Add(scriptName);
		}
	}

    /// <summary>
    /// Removes the Script that matches the given ScriptName from the Focus Exclusion List.
    /// </summary>
    /// <param name="scriptName">The name of the Script you want to remove from the Focus Exclusion List</param>
	public static void RemoveFocusExclusion(string scriptName)
	{
		FocusExclusions.Remove(scriptName);
	}

    /// <summary>
    /// Adds the Script that matches the given ScriptName to the History Exclusion List. That Script won't be accessable via the EnablePreviousScript() Method.
    /// </summary>
    /// <param name="scriptName">The name of the Script you want to add to the History Exclusion List</param>
	public static void AddHistoryExclusion(string scriptName)
	{
		if(!HistoryExclusions.Contains(scriptName))
		{
			HistoryExclusions.Add(scriptName);
		}
	}

    /// <summary>
    /// Removes the Script that matches the given ScriptName from the History Exclusion List.
    /// </summary>
    /// <param name="scriptName">The name of the Script you want to remove from the History Exclusion List</param>
	public static void RemoveHistoryExclusion(string scriptName)
	{
		HistoryExclusions.Remove(scriptName);
	}
	
	#endregion
	
	#endregion
}