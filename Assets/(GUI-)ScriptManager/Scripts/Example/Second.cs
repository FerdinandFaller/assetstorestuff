using UnityEngine;


public class Second : MonoBehaviour 
{
	public bool EnabledByOverlay;
	
	
	public void OnGUI()
	{
		GUI.Label(new Rect(100, 100, 200, 100), "SECOND");
		
		if(ScriptManager.GetFocusScript() == "Second")
		{
			GUI.Label(new Rect(100, 200, 200, 100), "I only render if this script has the focus");
		}
		
		if(EnabledByOverlay)
		{
			GUI.Label(new Rect(100, 250, 200, 100), "I was enabled by the overlay via direct access to the 'Second' Script");
		}
		
		// Activate Overlay
		if(GUI.Button(new Rect(400, 100, 100, 100), "Overlayswitch"))
		{
			if(ScriptManager.IsScriptEnabled("Overlay"))
			{
				ScriptManager.DisableScript("Overlay");
			}
			else
			{
				ScriptManager.EnableScript("Overlay");
			}
		}
		
		// Switch to first Example Script
		if(GUI.Button(new Rect(100, 300, 100, 100), "To First"))
		{
			ScriptManager.EnableScript("First");
			ScriptManager.DisableScript("Second");
			ScriptManager.DisableScript("Overlay");
		}
		
		// Switch to third Example Script
		if(GUI.Button(new Rect(400, 300, 100, 100), "To Third"))
		{
			ScriptManager.EnableScript("Third");
			ScriptManager.DisableScript("Second");
			ScriptManager.DisableScript("Overlay");
		}
	}
}