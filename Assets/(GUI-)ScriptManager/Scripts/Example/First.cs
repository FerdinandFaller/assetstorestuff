using UnityEngine;


public class First : MonoBehaviour 
{
	public void Start()
	{
		// We dont want to be able to revert to the overlay, so we exclude it from the history
		ScriptManager.AddHistoryExclusion("Overlay");
	}
	
	public void OnGUI()
	{
		GUI.Label(new Rect(100, 100, 200, 100), "FIRST");
		
		// Switch to second Example Script
		if(GUI.Button(new Rect(250, 300, 100, 100), "To Second"))
		{
			ScriptManager.EnableScript("Second");
			ScriptManager.DisableScript("First");
			ScriptManager.DisableScript("Overlay");
		}
		
		// Switch to third Example Script
		if(GUI.Button(new Rect(400, 300, 100, 100), "To Third"))
		{
			ScriptManager.EnableScript("Third");
			ScriptManager.DisableScript("First");
			ScriptManager.DisableScript("Overlay");
		}
	}
}