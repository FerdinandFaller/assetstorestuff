using UnityEngine;


public class Overlay : MonoBehaviour 
{
	public void OnGUI()
	{
		GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "Overlay");
		
		if(ScriptManager.IsScriptEnabled("Second"))
		{
			// Here we access the Script directly
			ScriptManager.GetEnabledScript<Second>().EnabledByOverlay = true;
		}
	}
}