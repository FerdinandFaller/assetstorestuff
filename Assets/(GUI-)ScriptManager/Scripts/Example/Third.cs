using UnityEngine;


public class Third : MonoBehaviour 
{
	public void OnGUI()
	{
		GUI.Label(new Rect(100, 100, 200, 100), "THIRD");
		
		
		// Go back to where we came from
		if(GUI.Button(new Rect(250, 400, 100, 100), "To Previous"))
		{
			ScriptManager.EnablePreviousScript();
			ScriptManager.DisableScript("Third");
		}
	}
}