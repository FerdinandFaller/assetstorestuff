using UnityEngine;

public class Example : MonoBehaviour
{
	private const float SlowDownFactor = 10;
	private readonly Vector2 _itemAmount = new Vector2 (10, 0);
	private readonly Vector2 _singleItemSize = new Vector2 (10, 10);
	private Swipe _swipeControl;
	
	// The "outer" Rectangle (The Area you can see)
	private readonly Rect _position = new Rect (10, 10, 10, 10);
	
	// The "inner" Rectangle (The Area that is inside the Area you can see, and that gets scrolled eventually)
	private readonly Rect _viewRect = new Rect (10, 10, 10, 10);

	
	
	private void Start ()
	{
		_swipeControl = new Swipe (_position, _singleItemSize, _itemAmount, SlowDownFactor);
	}


	private void Update ()
	{
		_swipeControl.Update ();
	}


	private void OnGUI ()
	{
		_swipeControl.ScrollPosition = GUI.BeginScrollView (_position, _swipeControl.ScrollPosition, _viewRect, "NoScrollBarsStyle", "NoScrollBarsStyle");
	}
}
