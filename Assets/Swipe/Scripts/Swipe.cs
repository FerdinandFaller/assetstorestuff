using UnityEngine;

public class Swipe
{
    private readonly float _slowdownFactor;
    private Vector2 _itemAmount;
    private Vector2 _itemSize;
    private Rect _scrollArea;
    private Vector2 _scrollPosition;
    private Vector2 _scrollVelocity;
    private Vector2 _selected = new Vector2(-1, -1);

    private Vector2 _targetScrollPosition;


    public Swipe(Rect scrollArea, Vector2 itemSize, Vector2 itemAmount, float slowDownFactor)
    {
        _scrollArea = scrollArea;
        _itemSize = itemSize;
        _itemAmount = itemAmount;
        _slowdownFactor = slowDownFactor;
    }

    public Vector2 ScrollPosition
    {
        get { return _scrollPosition; }
        set { _scrollPosition = value; }
    }


    /// <summary>
    /// This should be called in the Unity Update() Function
    /// </summary>
    public void Update()
    {
        // No touch or Mousepressing atm ?
        if (Input.touchCount == 0 || !Input.GetMouseButtonDown(0))
        {
            // Reset selected
            _selected = new Vector2(-1, -1);

            // v = ((v * (N - 1)) + w) / N
            // v = Current Value
            // w = Target Value
            // N = Slowdown Factor
            _scrollPosition.x = ((ScrollPosition.x*(_slowdownFactor - 1)) + _targetScrollPosition.x)/_slowdownFactor;
            _scrollPosition.y = ((ScrollPosition.y*(_slowdownFactor - 1)) + _targetScrollPosition.y)/_slowdownFactor;
        }
        else if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    {
                        _selected = TouchToItemIndex(touch.position);
                        _scrollVelocity = Vector2.zero;
                    }
                    break;


                case TouchPhase.Canceled:
                    {
                        _selected = new Vector2(-1, -1);
                    }
                    break;


                case TouchPhase.Moved:
                    {
                        _selected = new Vector2(-1, -1);

                        _scrollPosition.x -= touch.deltaPosition.x;
                        _scrollPosition.y += touch.deltaPosition.y;

                        _scrollVelocity.x = touch.deltaPosition.x/touch.deltaTime;
                        _scrollVelocity.y = touch.deltaPosition.y/touch.deltaTime;
                    }
                    break;


                case TouchPhase.Ended:
                    {
                        _targetScrollPosition = new Vector2(ScrollPosition.x + (_scrollVelocity.x*-0.5f),
                                                            ScrollPosition.y + (_scrollVelocity.y*0.5f));

                        // Was it a tap, or a drag-release?
                        if (_selected.x > -1 && _selected.y > -1)
                        {
                            Debug.Log("Player selected row " + _selected);
                        }
                    }
                    break;
            }
        }
        /* TODO: Implement Mouse
		if(Input.GetMouseButton(0))
		{
			// Here we could implement swiping via mouse. 
			// Atm this is only faking a selection.
			_selected = Vector2(1,1);
		}
		*/
    }


    private Vector2 TouchToItemIndex(Vector2 touchPos)
    {
        // invert coordinates
        var temp = new Vector2(Screen.width - touchPos.x, Screen.height - touchPos.y);

        // adjust for scroll position
        temp += ScrollPosition;

        // adjust for scrolling list offset
        temp.x -= _scrollArea.x;
        temp.y -= _scrollArea.y;

        Vector2 index;
        index.x = temp.x/_itemSize.x;
        index.y = temp.y/_itemSize.y;

        // they might have touched beyond last row
        index.y = Mathf.Min(index.y, _itemAmount.y);
        index.x = Mathf.Min(index.x, _itemAmount.x);

        return index;
    }


    private Vector2 ClickToItemIndex(Vector2 clickPos)
    {
        //throw new NotImplementedException();

        return Vector2.zero;
    }
}